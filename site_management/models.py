from django.db import models


class SingletonModel(models.Model):
    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.pk = 1
        super(SingletonModel, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        pass

    @classmethod
    def load(cls):
        obj, created = cls.objects.get_or_create(pk=1)
        return obj


class BestTourText(SingletonModel):
    title = models.CharField(
        verbose_name='Заголовок',
        max_length=255
    )
    content = models.TextField(
        verbose_name='Контент'
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Горящие туры (описание)'
        verbose_name_plural = 'Горящие туры (описание)'


class AboutUsText(SingletonModel):
    title = models.CharField(
        verbose_name='Заголовок',
        max_length=255
    )
    content = models.TextField(
        verbose_name='Контент'
    )
    photo = models.ImageField(
        verbose_name='Фотография',
        upload_to='about_us_photo/',
        null=True,
        blank=True
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Блог'
        verbose_name_plural = 'Блог'


class ContactUsText(SingletonModel):
    content = models.TextField(
        verbose_name='Контент'
    )

    class Meta:
        verbose_name = 'Свяжитесь с нами'
        verbose_name_plural = 'Свяжитесь с нами'


class ToursHeader(SingletonModel):
    photo = models.ImageField(
        verbose_name='Фотография',
        upload_to='headers/'
    )

    def __str__(self):
        return self.photo.url

    class Meta:
        verbose_name = 'Заголовок страницы туров'
        verbose_name_plural = 'Заголовок страницы туров'


class Visa(SingletonModel):
    text = models.TextField(
        verbose_name='Текст',
    )
    image = models.ImageField(
        verbose_name='Фотография'
    )

    def __str__(self):
        return self.image.url

    class Meta:
        verbose_name = 'Визовая поддержка'
        verbose_name_plural = 'Визовая поддержка'


class Workers(models.Model):
    title = models.CharField(
        verbose_name='Имя',
        max_length=255
    )
    role = models.CharField(
        verbose_name='Роль',
        max_length=255
    )
    image = models.ImageField(
        verbose_name='Фотография'
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Наши специалисты'
        verbose_name_plural = 'Наши специалисты'


class MainImage(SingletonModel):
    title = models.CharField(
        verbose_name='Заголовок',
        max_length=255
    )
    description = models.CharField(
        verbose_name='Описание',
        max_length=255
    )
    image = models.ImageField(
        verbose_name='Фотография',
        upload_to='main_image/'
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Главная картинка приветствия'
        verbose_name_plural = 'Главная картинка приветствия'
